let trainer = {
    Name: 'Ask Ketchum',
    Age: 18,
    Friends: [{
        kanto: ['Brock', 'Misty'],
        hoenn: ['May', 'Max']
    }],
    talk: () => {
        console.log('Pikachu! I choose you!');
    },
    Pokemons: []
}

trainer.Pokemons.push(new Pokemon('Pikachu', 8, 108, 18));
trainer.Pokemons.push(new Pokemon('Venasaur', 8, 208, 38));
trainer.Pokemons.push(new Pokemon('Charizard', 28, 308, 58));
trainer.Pokemons.push(new Pokemon('Blastoise', 8, 158, 48));

function Pokemon(name, level, health, attack) {
    this.Name = name;
    this.Level = level;
    this.Health = health;
    this.Attack = attack;
    this.Tackle = (target) => {
        if (target.Health <= 0) {
            this.Fainted(target, false);
        }
        else if (this.Health <= 0){
            this.Fainted(target, true);
        }
        else {
            let newHealth = target.Health - this.Attack;
            console.log(newHealth);

            console.log(`${this.Name} tackled ${target.Name} and inflicted ${this.Attack} damage.`);

            target.Health = newHealth;

            if (newHealth <= 0) {
                console.log(`${target.Name}'s health is now reduced to 0.`);
                this.Fainted(target);
            } else {
                console.log(`${target.Name}'s health is now reduced to ${target.health}.`);
            }
        }
    };
    this.Fainted = (target, isAttackerFainted) => {
        if (!isAttackerFainted){
            console.log(`${target.Name} has fainted.`);
        } else {
            console.log(`${this.Name} has fainted and can no longer attack.`);
        }
    };
}

console.log(trainer);

trainer.talk();

trainer.Pokemons[2].Tackle(trainer.Pokemons[0]);
trainer.Pokemons[2].Tackle(trainer.Pokemons[0]);
trainer.Pokemons[0].Tackle(trainer.Pokemons[2]);
trainer.Pokemons[3].Tackle(trainer.Pokemons[0]);